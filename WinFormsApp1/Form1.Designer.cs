﻿namespace WinFormsApp1
{
    partial class Form1
    {
        /// <summary>
        ///  Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        ///  Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        ///  Required method for Designer support - do not modify
        ///  the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            components = new System.ComponentModel.Container();
            tabControl = new TabControl();
            car_brand = new TabPage();
            label1 = new Label();
            textBoxCarBrand = new TextBox();
            car_model = new TabPage();
            comboBoxCarModel = new ComboBox();
            label3 = new Label();
            label2 = new Label();
            textBoxCarModel = new TextBox();
            sex = new TabPage();
            label4 = new Label();
            textBoxSex = new TextBox();
            person = new TabPage();
            dateTimePickerPerson = new DateTimePicker();
            label9 = new Label();
            label8 = new Label();
            textBoxMiddleName = new TextBox();
            label7 = new Label();
            textBoxFirstName = new TextBox();
            comboBoxSex = new ComboBox();
            label5 = new Label();
            label6 = new Label();
            textBoxLastName = new TextBox();
            button_add = new Button();
            imageList1 = new ImageList(components);
            dataGridView = new DataGridView();
            button_update = new Button();
            button_remove = new Button();
            tabControl.SuspendLayout();
            car_brand.SuspendLayout();
            car_model.SuspendLayout();
            sex.SuspendLayout();
            person.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)dataGridView).BeginInit();
            SuspendLayout();
            // 
            // tabControl
            // 
            tabControl.Controls.Add(car_brand);
            tabControl.Controls.Add(car_model);
            tabControl.Controls.Add(sex);
            tabControl.Controls.Add(person);
            tabControl.Location = new Point(498, -3);
            tabControl.Name = "tabControl";
            tabControl.SelectedIndex = 0;
            tabControl.Size = new Size(347, 314);
            tabControl.TabIndex = 0;
            tabControl.TabStop = false;
            tabControl.SelectedIndexChanged += tabControl_SelectedIndexChanged;
            // 
            // car_brand
            // 
            car_brand.Controls.Add(label1);
            car_brand.Controls.Add(textBoxCarBrand);
            car_brand.Location = new Point(4, 24);
            car_brand.Name = "car_brand";
            car_brand.Padding = new Padding(3);
            car_brand.Size = new Size(339, 286);
            car_brand.TabIndex = 0;
            car_brand.Text = "Car brand";
            car_brand.UseVisualStyleBackColor = true;
            // 
            // label1
            // 
            label1.AutoSize = true;
            label1.Location = new Point(6, 137);
            label1.Name = "label1";
            label1.Size = new Size(92, 15);
            label1.TabIndex = 3;
            label1.Text = "Name car Brand";
            // 
            // textBoxCarBrand
            // 
            textBoxCarBrand.Location = new Point(134, 134);
            textBoxCarBrand.Name = "textBoxCarBrand";
            textBoxCarBrand.Size = new Size(192, 23);
            textBoxCarBrand.TabIndex = 1;
            // 
            // car_model
            // 
            car_model.Controls.Add(comboBoxCarModel);
            car_model.Controls.Add(label3);
            car_model.Controls.Add(label2);
            car_model.Controls.Add(textBoxCarModel);
            car_model.Location = new Point(4, 24);
            car_model.Name = "car_model";
            car_model.Padding = new Padding(3);
            car_model.Size = new Size(339, 286);
            car_model.TabIndex = 1;
            car_model.Text = "Car model";
            car_model.UseVisualStyleBackColor = true;
            // 
            // comboBoxCarModel
            // 
            comboBoxCarModel.FormattingEnabled = true;
            comboBoxCarModel.Location = new Point(141, 154);
            comboBoxCarModel.Name = "comboBoxCarModel";
            comboBoxCarModel.Size = new Size(185, 23);
            comboBoxCarModel.TabIndex = 6;
            comboBoxCarModel.SelectedIndexChanged += comboBoxCarModel_SelectedIndexChanged;
            // 
            // label3
            // 
            label3.AutoSize = true;
            label3.Location = new Point(11, 93);
            label3.Name = "label3";
            label3.Size = new Size(95, 15);
            label3.TabIndex = 5;
            label3.Text = "Name car model";
            // 
            // label2
            // 
            label2.AutoSize = true;
            label2.Location = new Point(14, 162);
            label2.Name = "label2";
            label2.Size = new Size(92, 15);
            label2.TabIndex = 4;
            label2.Text = "Name car Brand";
            // 
            // textBoxCarModel
            // 
            textBoxCarModel.Location = new Point(141, 90);
            textBoxCarModel.Name = "textBoxCarModel";
            textBoxCarModel.Size = new Size(185, 23);
            textBoxCarModel.TabIndex = 2;
            // 
            // sex
            // 
            sex.Controls.Add(label4);
            sex.Controls.Add(textBoxSex);
            sex.Location = new Point(4, 24);
            sex.Name = "sex";
            sex.Padding = new Padding(3);
            sex.Size = new Size(339, 286);
            sex.TabIndex = 2;
            sex.Text = "Sex";
            sex.UseVisualStyleBackColor = true;
            // 
            // label4
            // 
            label4.AutoSize = true;
            label4.Location = new Point(52, 135);
            label4.Name = "label4";
            label4.Size = new Size(25, 15);
            label4.TabIndex = 4;
            label4.Text = "Sex";
            // 
            // textBoxSex
            // 
            textBoxSex.Location = new Point(118, 132);
            textBoxSex.Name = "textBoxSex";
            textBoxSex.Size = new Size(192, 23);
            textBoxSex.TabIndex = 2;
            // 
            // person
            // 
            person.Controls.Add(dateTimePickerPerson);
            person.Controls.Add(label9);
            person.Controls.Add(label8);
            person.Controls.Add(textBoxMiddleName);
            person.Controls.Add(label7);
            person.Controls.Add(textBoxFirstName);
            person.Controls.Add(comboBoxSex);
            person.Controls.Add(label5);
            person.Controls.Add(label6);
            person.Controls.Add(textBoxLastName);
            person.Location = new Point(4, 24);
            person.Name = "person";
            person.Padding = new Padding(3);
            person.Size = new Size(339, 286);
            person.TabIndex = 3;
            person.Text = "Person";
            person.UseVisualStyleBackColor = true;
            // 
            // dateTimePickerPerson
            // 
            dateTimePickerPerson.Location = new Point(142, 171);
            dateTimePickerPerson.Name = "dateTimePickerPerson";
            dateTimePickerPerson.Size = new Size(184, 23);
            dateTimePickerPerson.TabIndex = 17;
            // 
            // label9
            // 
            label9.AutoSize = true;
            label9.Location = new Point(12, 171);
            label9.Name = "label9";
            label9.Size = new Size(58, 15);
            label9.TabIndex = 16;
            label9.Text = "Birth date";
            // 
            // label8
            // 
            label8.AutoSize = true;
            label8.Location = new Point(12, 77);
            label8.Name = "label8";
            label8.Size = new Size(77, 15);
            label8.TabIndex = 14;
            label8.Text = "Middle name";
            // 
            // textBoxMiddleName
            // 
            textBoxMiddleName.Location = new Point(142, 74);
            textBoxMiddleName.Name = "textBoxMiddleName";
            textBoxMiddleName.Size = new Size(185, 23);
            textBoxMiddleName.TabIndex = 13;
            // 
            // label7
            // 
            label7.AutoSize = true;
            label7.Location = new Point(12, 33);
            label7.Name = "label7";
            label7.Size = new Size(62, 15);
            label7.TabIndex = 12;
            label7.Text = "First name";
            // 
            // textBoxFirstName
            // 
            textBoxFirstName.Location = new Point(142, 30);
            textBoxFirstName.Name = "textBoxFirstName";
            textBoxFirstName.Size = new Size(185, 23);
            textBoxFirstName.TabIndex = 11;
            // 
            // comboBoxSex
            // 
            comboBoxSex.FormattingEnabled = true;
            comboBoxSex.Location = new Point(141, 212);
            comboBoxSex.Name = "comboBoxSex";
            comboBoxSex.Size = new Size(185, 23);
            comboBoxSex.TabIndex = 10;
            // 
            // label5
            // 
            label5.AutoSize = true;
            label5.Location = new Point(12, 121);
            label5.Name = "label5";
            label5.Size = new Size(61, 15);
            label5.TabIndex = 9;
            label5.Text = "Last name";
            // 
            // label6
            // 
            label6.AutoSize = true;
            label6.Location = new Point(11, 215);
            label6.Name = "label6";
            label6.Size = new Size(25, 15);
            label6.TabIndex = 8;
            label6.Text = "Sex";
            // 
            // textBoxLastName
            // 
            textBoxLastName.Location = new Point(142, 118);
            textBoxLastName.Name = "textBoxLastName";
            textBoxLastName.Size = new Size(185, 23);
            textBoxLastName.TabIndex = 7;
            // 
            // button_add
            // 
            button_add.Location = new Point(513, 317);
            button_add.Name = "button_add";
            button_add.Size = new Size(315, 43);
            button_add.TabIndex = 4;
            button_add.Text = "Add";
            button_add.UseVisualStyleBackColor = true;
            button_add.Click += buttonAddClick;
            // 
            // imageList1
            // 
            imageList1.ColorDepth = ColorDepth.Depth8Bit;
            imageList1.ImageSize = new Size(16, 16);
            imageList1.TransparentColor = Color.Transparent;
            // 
            // dataGridView
            // 
            dataGridView.ColumnHeadersHeightSizeMode = DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            dataGridView.Location = new Point(0, -3);
            dataGridView.Name = "dataGridView";
            dataGridView.RowTemplate.Height = 25;
            dataGridView.Size = new Size(496, 470);
            dataGridView.TabIndex = 4;
            // 
            // button_update
            // 
            button_update.Location = new Point(513, 366);
            button_update.Name = "button_update";
            button_update.Size = new Size(315, 43);
            button_update.TabIndex = 5;
            button_update.Text = "Update";
            button_update.UseVisualStyleBackColor = true;
            button_update.Click += buttonUpdateClick;
            // 
            // button_remove
            // 
            button_remove.Location = new Point(513, 415);
            button_remove.Name = "button_remove";
            button_remove.Size = new Size(315, 43);
            button_remove.TabIndex = 6;
            button_remove.Text = "Remove";
            button_remove.UseVisualStyleBackColor = true;
            button_remove.Click += buttonRemoveClick;
            // 
            // Form1
            // 
            AutoScaleDimensions = new SizeF(7F, 15F);
            AutoScaleMode = AutoScaleMode.Font;
            ClientSize = new Size(841, 466);
            Controls.Add(button_remove);
            Controls.Add(button_update);
            Controls.Add(button_add);
            Controls.Add(dataGridView);
            Controls.Add(tabControl);
            Name = "Form1";
            Text = "Car salon";
            Click += FormClick;
            tabControl.ResumeLayout(false);
            car_brand.ResumeLayout(false);
            car_brand.PerformLayout();
            car_model.ResumeLayout(false);
            car_model.PerformLayout();
            sex.ResumeLayout(false);
            sex.PerformLayout();
            person.ResumeLayout(false);
            person.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)dataGridView).EndInit();
            ResumeLayout(false);
        }

        #endregion
        private TabPage car_brand;
        private TabPage car_model;
        private TabPage sex;
        private TabPage person;
        private ImageList imageList1;
        private TextBox textBoxCarBrand;
        private Label label1;
        private DataGridView dataGridView;
        private Button button_add;
        private Button button_update;
        private Button button_remove;
        protected internal TabControl tabControl;
        private Label label3;
        private Label label2;
        private TextBox textBoxCarModel;
        private ComboBox comboBoxCarModel;
        private Label label4;
        private TextBox textBoxSex;
        private Label label8;
        private TextBox textBoxMiddleName;
        private Label label7;
        private TextBox textBoxFirstName;
        private ComboBox comboBoxSex;
        private Label label5;
        private Label label6;
        private TextBox textBoxLastName;
        private Label label9;
        private DateTimePicker dateTimePickerPerson;
    }
}