﻿using System.ComponentModel.DataAnnotations;



namespace WinFormsApp1.Models
{
    public class CarModel
    {
        [Key]
        public int IdCarModel { get; set; }

        public int IdCarBrand { get; set; }

        [MaxLength(50)]
        public string NameCarModel { get; set; }

        // public CarBrand CarBrand { get; set; }
    }
}
