﻿using System.ComponentModel.DataAnnotations;

namespace WinFormsApp1.Models
{
    public class Person
    {
        [Key]
        public int IdPerson { get; set; }

        [MaxLength(50)]
        [Required]
        public string FirstName { get; set; }

        [MaxLength(50)]
        public string MiddleName { get; set; }

        [MaxLength(50)]
        [Required]
        public string LastName { get; set; }

        public DateTime? BirthDate { get; set; }
        public int IdSex { get; set; }
        //public Sex Sex { get; set; }
    }
}
