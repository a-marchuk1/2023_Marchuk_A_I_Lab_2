﻿using System.ComponentModel.DataAnnotations;

namespace WinFormsApp1.Models
{
    public class CarBrand
    {
        [Key]
        public int IdCarBrand { get; set; }

        [MaxLength(50)]
        public string NameCarBrand { get; set; }

        //public ICollection<CarModel> CarModels { get; set; }
    }

}
