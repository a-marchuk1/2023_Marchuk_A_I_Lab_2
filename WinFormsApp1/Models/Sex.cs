﻿using System.ComponentModel.DataAnnotations;

namespace WinFormsApp1.Models
{
    public class Sex
    {
        [Key]
        public int IdSex { get; set; }

        [MaxLength(20)]
        [Required]
        public string Description { get; set; }

        //public ICollection<Person> People { get; set; }
    }
}
