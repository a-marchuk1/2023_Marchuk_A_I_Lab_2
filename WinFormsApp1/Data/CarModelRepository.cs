﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WinFormsApp1.Models;

namespace WinFormsApp1.Data
{
    public class CarModelRepository
    {
        private SalonContext context;

        public CarModelRepository(SalonContext context)
        {
            this.context = context;
        }

        public void AddCarModel(string name, int carBrandId)
        {
            var carModel = new CarModel
            {
                NameCarModel = name,
                IdCarBrand = carBrandId
            };
            context.CarModels.Add(carModel);
            context.SaveChanges();
        }

        public void UpdateCarModel(string carModelName, string newCarModelName, string newCarBrandName)
        {
            var carModel = context.CarModels.FirstOrDefault(m => m.NameCarModel == carModelName);
            if (carModel != null)
            {
                var carBrand = context.CarBrands.FirstOrDefault(b => b.NameCarBrand == newCarBrandName);
                if (carBrand != null)
                {
                    carModel.NameCarModel = newCarModelName;
                    carModel.IdCarBrand = carBrand.IdCarBrand;
                    context.SaveChanges();
                }
            }
        }


        // Додайте інші методи, які потрібні для роботи з моделями автомобілів
    }

}
