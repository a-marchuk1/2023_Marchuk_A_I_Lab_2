﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WinFormsApp1.Models;

namespace WinFormsApp1.Data
{
    public class SexRepository
    {
        private SalonContext context;

        public SexRepository(SalonContext context)
        {
            this.context = context;
        }

        public int GetSexIdByDescription(string description)
        {
            return context.Sexes
                .Where(s => s.Description == description)
                .Select(s => s.IdSex)
                .FirstOrDefault();
        }


        public void AddSex(string description)
        {
            var sex = new Sex { Description = description };
            context.Sexes.Add(sex);
            context.SaveChanges();
        }

        public void UpdateSex(string sexDescription, string newSexDescription)
        {
            var sex = context.Sexes.FirstOrDefault(s => s.Description == sexDescription);
            if (sex != null)
            {
                sex.Description = newSexDescription;
                context.SaveChanges();
            }
        }


        // Додайте інші методи, які потрібні для роботи зі статтю
    }

}
