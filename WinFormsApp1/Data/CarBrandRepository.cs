﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WinFormsApp1.Models;

namespace WinFormsApp1.Data
{
    public class CarBrandRepository
    {
        private SalonContext context;

        public CarBrandRepository(SalonContext context)
        {
            this.context = context;
        }

        public void AddCarBrand(string name)
        {
            var carBrand = new CarBrand { NameCarBrand = name };
            context.CarBrands.Add(carBrand);
            context.SaveChanges();
        }

        public void UpdateCarBrand(string currentName, string newName)
        {
            var carBrand = context.CarBrands.FirstOrDefault(b => b.NameCarBrand == currentName);
            if (carBrand != null)
            {
                carBrand.NameCarBrand = newName;
                context.SaveChanges();
            }
        }

        // Додайте інші методи, які потрібні для роботи з марками автомобілів
    }

}
