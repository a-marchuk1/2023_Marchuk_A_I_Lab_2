﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WinFormsApp1.Models;

namespace WinFormsApp1.Data
{
    public class PersonRepository
    {
        private SalonContext context;

        public PersonRepository(SalonContext context)
        {
            this.context = context;
        }

        public int GetPersonIdByNameAndBirthDate(string firstName, string lastName, DateTime birthDate)
        {
            return context.People
                .Where(p => p.FirstName == firstName && p.LastName == lastName && p.BirthDate == birthDate)
                .Select(p => p.IdPerson)
                .FirstOrDefault();
        }


        public void AddPerson(string firstName, string middleName, string lastName, DateTime birthDate, int sexId)
        {
            var person = new Person
            {
                FirstName = firstName,
                MiddleName = middleName,
                LastName = lastName,
                BirthDate = birthDate,
                IdSex = sexId
            };
            context.People.Add(person);
            context.SaveChanges();
        }

        public void UpdatePerson(int personId, string newFirstName, string newMiddleName, string newLastName, DateTime newBirthDate, int newSexId)
        {
            var person = context.People.FirstOrDefault(p => p.IdPerson == personId);
            if (person != null)
            {
                person.FirstName = newFirstName;
                person.MiddleName = newMiddleName;
                person.LastName = newLastName;
                person.BirthDate = newBirthDate;
                person.IdSex = newSexId;
                context.SaveChanges();
            }
        }



        // Додайте інші методи, які потрібні для роботи з людьми
    }

}
