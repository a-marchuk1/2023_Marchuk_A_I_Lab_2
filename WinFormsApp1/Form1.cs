using WinFormsApp1.Models;

namespace WinFormsApp1
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
            Load += Form1_Load;

            Form1_Load(this, EventArgs.Empty);
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            using (var context = new SalonContext())
            {
                context.Database.EnsureCreated();
                context.SaveChanges();
            }

            tabControl_SelectedIndexChanged(sender, e);
        }

        //������ �������� tabControl_SelectedIndexChanged ��� ���������� �� ����� ��� ��������� ����������
        private void FormClick(object sender, EventArgs e)
        {
            tabControl_SelectedIndexChanged(sender, e);
        }

        private void LoadComboBoxes()
        {
            using (var context = new SalonContext())
            {
                var carBrands = context.CarBrands.ToList();
                var sexOptions = context.Sexes.ToList();

                comboBoxCarModel.DataSource = carBrands;
                comboBoxCarModel.DisplayMember = "NameCarBrand";
                comboBoxCarModel.ValueMember = "IdCarBrand";
              
                comboBoxSex.DataSource = sexOptions;
                comboBoxSex.DisplayMember = "Description"; 
                comboBoxSex.ValueMember = "IdSex";
            }
        }



        // ��������� ������ ��� ���������� �� ������� tabControll
        private void tabControl_SelectedIndexChanged(object sender, EventArgs e)
        {
            using (var context = new SalonContext())
            {
                if (tabControl.SelectedIndex == 0)
                {
                    // �������� �� ���������� ���� ��� CarBrand
                    var carBrands = context.CarBrands.ToList();
                    dataGridView.DataSource = carBrands;
                }
                else if (tabControl.SelectedIndex == 1)
                {
                    // �������� �� ���������� ���� ��� CarModel
                    var carModels = context.CarModels.ToList();
                    dataGridView.DataSource = carModels;
                    LoadComboBoxes();

                }
                else if (tabControl.SelectedIndex == 2)
                {
                    // �������� �� ���������� ���� ��� Sex
                    var sexes = context.Sexes.ToList();
                    dataGridView.DataSource = sexes;
                }
                else if (tabControl.SelectedIndex == 3)
                {
                    // �������� �� ���������� ���� ��� Person
                    var people = context.People.ToList();
                    dataGridView.DataSource = people;
                    LoadComboBoxes();
                }
            }
        }

        private void buttonRemoveClick(object sender, EventArgs e)
        {
            using (var context = new SalonContext())
            {
                if (tabControl.SelectedIndex == 0)
                {
                    // �������� ���� � textBoxCarBrand � �������� CarBrand � ���� �����
                    string carBrandName = textBoxCarBrand.Text;
                    var carBrand = context.CarBrands.FirstOrDefault(b => b.NameCarBrand == carBrandName);
                    if (carBrand != null)
                    {
                        context.CarBrands.Remove(carBrand);
                    }
                    context.SaveChanges();
                    ClearTextBoxes(); // �������� ������� ����
                }
                else if (tabControl.SelectedIndex == 1)
                {
                    // �������� ���� � textBoxCarModel �� �������� CarModel � ���� �����
                    string carModelName = textBoxCarModel.Text;
                    var carModel = context.CarModels.FirstOrDefault(m => m.NameCarModel == carModelName);
                    if (carModel != null)
                    {
                        context.CarModels.Remove(carModel);
                    }
                    context.SaveChanges();
                    ClearTextBoxes(); // �������� ������� ����
                }
                else if (tabControl.SelectedIndex == 2)
                {
                    // �������� ���� � textBoxSex � �������� Sex � ���� �����
                    string sexDescription = textBoxSex.Text;
                    var sex = context.Sexes.FirstOrDefault(s => s.Description == sexDescription);
                    if (sex != null)
                    {
                        context.Sexes.Remove(sex);
                    }
                    context.SaveChanges();
                    ClearTextBoxes(); // �������� ������� ����
                }
                else if (tabControl.SelectedIndex == 3)
                {
                    // �������� ���� ��� ��������� Person � ���� �����
                    string firstName = textBoxFirstName.Text;
                    string lastName = textBoxLastName.Text;
                    var person = context.People.FirstOrDefault(p => p.FirstName == firstName && p.LastName == lastName);
                    if (person != null)
                    {
                        context.People.Remove(person);
                    }
                    context.SaveChanges();
                    ClearTextBoxes(); // �������� ������� ����
                }
            }
            tabControl_SelectedIndexChanged(sender, e);
        }
        private string originalCarBrandName;
        private string originalCarModelName;
        private string originalCarModelBrand;
        private string originalSexDescription;
        private string originalFirstName;
        private string originalLastName;
        private DateTime originalBirthDate;
        private void buttonUpdateClick(object sender, EventArgs e)
        {
            using (var context = new SalonContext())
            {
                if (tabControl.SelectedIndex == 0)
                {
                    string carBrandName = textBoxCarBrand.Text;
                    if (string.IsNullOrWhiteSpace(carBrandName))
                    {
                        MessageBox.Show("���� �����, ������ ��'� ����� ���������.");
                        return;
                    }

                    if (originalCarBrandName != carBrandName)
                    {
                        // ������� ��'� ����� ��������� � ��� �����
                        UpdateCarBrand(context, originalCarBrandName, carBrandName);
                        originalCarBrandName = carBrandName;
                        ClearTextBoxes(); // �������� ������� ����
                    }
                }
                else if (tabControl.SelectedIndex == 1)
                {
                    string carModelName = textBoxCarModel.Text;
                    string carModelBrand = comboBoxCarModel.Text;

                    if (string.IsNullOrWhiteSpace(carModelName))
                    {
                        MessageBox.Show("���� �����, ������ ��'� ����� ���������.");
                        return;
                    }

                    if (originalCarModelName != carModelName || originalCarModelBrand != carModelBrand)
                    {
                        // ������� ��'� ����� ��������� �� ����� � ��� �����
                        UpdateCarModel(context, originalCarModelName, carModelName, carModelBrand);
                        originalCarModelName = carModelName;
                        originalCarModelBrand = carModelBrand;
                        ClearTextBoxes(); // �������� ������� ����
                    }
                }
                else if (tabControl.SelectedIndex == 2)
                {
                    string sexDescription = textBoxSex.Text;

                    if (string.IsNullOrWhiteSpace(sexDescription))
                    {
                        MessageBox.Show("���� �����, ������ ���� ����.");
                        return;
                    }

                    if (originalSexDescription != sexDescription)
                    {
                        // ������� ���� ���� � ��� �����
                        UpdateSex(context, originalSexDescription, sexDescription);
                        originalSexDescription = sexDescription;
                        ClearTextBoxes(); // �������� ������� ����
                    }
                }
                else if (tabControl.SelectedIndex == 3)
                {
                    string firstName = textBoxFirstName.Text;
                    string lastName = textBoxLastName.Text;
                    DateTime birthDate = dateTimePickerPerson.Value;
                    string sexDescription = comboBoxSex.Text;

                    if (string.IsNullOrWhiteSpace(firstName) || string.IsNullOrWhiteSpace(lastName) ||
                        birthDate == originalBirthDate || string.IsNullOrWhiteSpace(sexDescription))
                    {
                        MessageBox.Show("���� �����, ��������� �� ����'����� ���� �� ������ ����.");
                        return;
                    }

                    if (originalFirstName != firstName || originalLastName != lastName ||
                        birthDate != originalBirthDate || originalSexDescription != sexDescription)
                    {
                        // ������� ���� Person � ��� �����
                        UpdatePerson(context, originalFirstName, originalLastName, firstName, lastName, birthDate, sexDescription);
                        originalFirstName = firstName;
                        originalLastName = lastName;
                        originalBirthDate = birthDate;
                        originalSexDescription = sexDescription;
                        ClearTextBoxes(); // �������� ������� ����
                    }
                }

                context.SaveChanges();
            }
            tabControl_SelectedIndexChanged(sender, e);
        }

        private void buttonAddClick(object sender, EventArgs e)
        {
            using (var context = new SalonContext())
            {
                if (tabControl.SelectedIndex == 0)
                {
                    // �������� ���� � textBoxCarBrand �� �������� CarBrand � �� 
                    string carBrandName = textBoxCarBrand.Text;
                    var newCarBrand = new CarBrand { NameCarBrand = carBrandName };
                    context.CarBrands.Add(newCarBrand);
                    context.SaveChanges();
                    ClearTextBoxes(); // �������� ������� ����
                }
                else if (tabControl.SelectedIndex == 1)
                {
                    // �������� ���� � textBoxCarModel �� comboBoxCarModel �� �������� CarModel � �� 
                    string carModelName = textBoxCarModel.Text;

                    // �������� ������� ����� ��������� �� comboBox
                    var selectedCarBrand = (CarBrand)comboBoxCarModel.SelectedItem;

                    if (selectedCarBrand != null)
                    {
                        var newCarModel = new CarModel
                        {
                            IdCarBrand = selectedCarBrand.IdCarBrand,
                            NameCarModel = carModelName,
                        };
                        context.CarModels.Add(newCarModel);
                        context.SaveChanges();
                        ClearTextBoxes(); // �������� ������� ����
                    }

                }
                else if (tabControl.SelectedIndex == 2)
                {
                    // �������� ���� � textBoxSex �� �������� Sex � �� 
                    string sexDescription = textBoxSex.Text;
                    var newSex = new Sex { Description = sexDescription };
                    context.Sexes.Add(newSex);
                    context.SaveChanges();
                    ClearTextBoxes(); // �������� ������� ����
                }
                else if (tabControl.SelectedIndex == 3)
                {
                    // �������� ���� ��� Person �� �������� Person � ��
                    string firstName = textBoxFirstName.Text;
                    string middleName = textBoxMiddleName.Text;
                    string lastName = textBoxLastName.Text;
                    DateTime birthDate = dateTimePickerPerson.Value;
                    var selectedSex = (Sex)comboBoxSex.SelectedItem;
                    if (selectedSex != null)
                    {
                        var newPerson = new Person
                        {
                            FirstName = firstName,
                            MiddleName = middleName,
                            LastName = lastName,
                            BirthDate = birthDate,
                            IdSex = selectedSex.IdSex 
                        };
                        context.People.Add(newPerson);
                        context.SaveChanges();
                        ClearTextBoxes(); // �������� ������� ����
                    }
                }
            }
            tabControl_SelectedIndexChanged(sender, e);
        }

        // ����� ��� �������� ��������� ����
        private void ClearTextBoxes()
        {
            textBoxCarBrand.Clear();
            textBoxCarModel.Clear();
            textBoxSex.Clear();
            textBoxFirstName.Clear();
            textBoxLastName.Clear();
            textBoxMiddleName.Clear();
            comboBoxCarModel.SelectedIndex = -1;
            comboBoxSex.SelectedIndex = -1;
            dateTimePickerPerson.Value = DateTime.Now;
        }


        //Update Methods
        private void UpdateCarBrand(SalonContext context, string carBrandName, string newCarBrandName)
        {
            var carBrand = context.CarBrands.FirstOrDefault(b => b.NameCarBrand == carBrandName);
            if (carBrand != null)
            {
                carBrand.NameCarBrand = newCarBrandName;
                context.SaveChanges();
            }
        }


        private void UpdateCarModel(SalonContext context, string carModelName, string newCarModelName, string newCarBrandName)
        {
            var carModel = context.CarModels.FirstOrDefault(m => m.NameCarModel == carModelName);
            if (carModel != null)
            {
                var carBrand = context.CarBrands.FirstOrDefault(b => b.NameCarBrand == newCarBrandName);
                if (carBrand != null)
                {
                    carModel.NameCarModel = newCarModelName;
                    carModel.IdCarBrand = carBrand.IdCarBrand;
                    context.SaveChanges();
                }
            }
        }

        private void UpdateSex(SalonContext context, string sexDescription, string newSexDescription)
        {
            var sex = context.Sexes.FirstOrDefault(s => s.Description == sexDescription);
            if (sex != null)
            {
                sex.Description = newSexDescription;
                context.SaveChanges();
            }
        }

        private void UpdatePerson(SalonContext context, string firstName, string lastName, string newFirstName, string newLastName, DateTime newBirthDate, string newSexDescription)
        {
            var person = context.People.FirstOrDefault(p => p.FirstName == firstName && p.LastName == lastName);
            if (person != null)
            {
                var sex = context.Sexes.FirstOrDefault(s => s.Description == newSexDescription);
                if (sex != null)
                {
                    person.FirstName = newFirstName;
                    person.LastName = newLastName;
                    person.BirthDate = newBirthDate;
                    person.IdSex = sex.IdSex;
                    context.SaveChanges();
                }
            }
        }

        private void comboBoxCarModel_SelectedIndexChanged(object sender, EventArgs e)
        {

        }
    }
}